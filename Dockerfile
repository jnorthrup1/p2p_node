FROM ocaml/opam:ubuntu-21.04-ocaml-4.12 AS builder

WORKDIR /src

RUN sudo rm /bin/sh && sudo ln -s /bin/bash /bin/sh

COPY . .

RUN sudo chmod 777 -R .
RUN sudo apt install -y git findutils
RUN sh install_docker.sh
RUN eval $(opam env)

# Store the dynamic dependencies of the server
RUN opam depext -ln lib_dht > /src/depexts_lib_dht

FROM ubuntu:21.04 AS app

WORKDIR /app

COPY --from=builder /src/opam-install/bin/node node
COPY --from=builder /src/depexts_lib_dht depexts_lib_dht

## for apt to be noninteractive
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

## preesed tzdata, update package index, upgrade packages and install needed software
RUN echo "tzdata tzdata/Areas select Europe" > /tmp/preseed.txt; \
    echo "tzdata tzdata/Zones/Europe select Paris" >> /tmp/preseed.txt; \
    debconf-set-selections /tmp/preseed.txt && \
    apt-get update && \
    apt-get install -y --no-install-recommends apt-utils && \
    # Install the required dynamic dependencies
    cat depexts_lib_dht | xargs apt-get install -y 

EXPOSE 10000
EXPOSE 30000

CMD ["./node"]
