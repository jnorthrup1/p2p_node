(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Messages
open Base

module Node = struct
  module T = struct
    type t = node [@@deriving sexp_of, compare]
  end

  include T
  include Comparable.Make (T)

  let ( = ) a b = Int.( = ) (compare a b) 0

  let ( <> ) a b = not (a = b)
end

module Node_info = struct
  module T = struct
    type t = {
      node : node;
      (* Duration in seconds since 00:00:00 GMT, Jan. 1, 1970. *)
      first_seen : float;
      (* Duration in seconds since 00:00:00 GMT, Jan. 1, 1970. *)
      last_seen : float;
    }
    [@@deriving sexp_of]

    let compare { node = a; _ } { node = b; _ } = Node.compare a b
  end

  include T
  include Comparable.Make (T)

  let encoding =
    let open Data_encoding in
    conv
      (fun ({ node = { id; ip_addr; port }; first_seen; last_seen } : T.t) ->
        (id, ip_addr, port, first_seen, last_seen))
      (fun (id, ip_addr, port, first_seen, last_seen) ->
        { node = { id; ip_addr; port }; first_seen; last_seen })
      (obj5
         (req "id" P2p_peer.Id.encoding)
         (req "ip_addr" P2p_addr.encoding)
         (req "port" int32) (req "first_seen" float) (req "last_seen" float))
end

module Table = struct
  type t = {
    owner : Node_id.t;
    contents :
      (int, (Node_info.t, Node_info.comparator_witness) Set.t) Hashtbl.t;
  }

  let create owner = { owner; contents = Hashtbl.create (module Int) }

  let delete_node { owner; contents } node =
    let pos = Node_id.bucket_index owner node.id in
    Hashtbl.update contents pos ~f:(function
      | Some nodes ->
          Set.filter nodes ~f:(fun { node = n; _ } ->
              Node_id.( <> ) n.id node.id)
      | None -> Set.empty (module Node_info))

  let nodes { contents; _ } =
    Hashtbl.fold contents ~init:[] ~f:(fun ~key:_ ~data l ->
        List.append l (Set.to_list data))

  let store_node ~net node ({ owner; contents } as t) =
    let open Base in
    (* If the node id correspond to the one from the current node, abort. *)
    if Node_id.(t.owner = node.id) then Lwt.return_unit
    else
      (* Index of the bucket which will store the node. *)
      let pos = Node_id.bucket_index owner node.id in
      let now = Unix.time () in

      (* Initialize node's entry in the routing table. *)
      let info : Node_info.t = { node; first_seen = now; last_seen = now } in

      let replace set elt = Set.add (Set.remove set elt) elt in

      (* Insert or update the node in the bucket, if possible. *)
      let (_ : unit) =
        Hashtbl.update contents pos ~f:(function
          | None -> Set.singleton (module Node_info) info
          | Some nodes when Set.length nodes < Constants.bucket_size ->
              (* If the node is already in the bucket, update its last seen date. *)
              replace nodes
                (match Set.find nodes ~f:(Node_info.( = ) info) with
                | None -> info
                | Some n -> { n with last_seen = now })
          | Some nodes -> nodes)
      in

      (* In case the bucket is full: *)
      match Hashtbl.find contents pos with
      | Some nodes when Set.length nodes = Constants.bucket_size -> (
          (* Query oldest contact to check if it is alive. *)
          match
            Set.to_list nodes
            |> List.sort ~compare:(fun a b ->
                   Float.compare a.last_seen b.last_seen)
            |> List.hd
          with
          | Some ({ node; _ } as n) -> (
              assert (Node.( = ) node n.node);
              Network.connect_to_node_res ~net ~node >>= function
              | Ok _ ->
                  (* Node is alive, update it. *)
                  Hashtbl.update contents pos ~f:(function
                    | Some nodes -> replace nodes { n with last_seen = now }
                    | None -> Set.empty (module Node_info));
                  Lwt.return_unit
              | Error _ ->
                  (* Node is dead or absent, delete it. *)
                  Lwt.return @@ delete_node t node)
          | None -> Lwt.return_unit)
      | _ -> Lwt.return_unit

  let buckets { contents; _ } = Hashtbl.keys contents

  let sorted_nodes t target =
    List.sort (nodes t) ~compare:(fun a b ->
        match
          Node_id.compare
            (Node_id.xor a.node.id target)
            (Node_id.xor b.node.id target)
        with
        | 0 -> -Float.compare a.last_seen b.last_seen
        | i -> i)

  let closest_nodes' t target ~n = sorted_nodes t target |> Misc.take ~n

  let closest_nodes = closest_nodes' ~n:Constants.bucket_size

  let bucket { contents; _ } i =
    Option.Monad_infix.(Hashtbl.find contents i >>| Set.to_list)

  let mem' l ~id = List.mem l id ~equal:Node_id.( = )

  let mem t id = nodes t |> List.map ~f:(fun { node; _ } -> node.id) |> mem' ~id
end
