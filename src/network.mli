(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Tezos_p2p
open Tezos_p2p_services

val init_node :
  config:P2p.config ->
  limits:P2p.limits ->
  msg_config:'a P2p_params.message_config ->
  ('a, Peer_metadata.t, Connection_metadata.t) P2p.t tzresult Lwt.t
(** Main node initialization function. It takes three records:
    - [config]: network configuration
    - [limits]: system limits used by the maintenance and welcome worker
    - [msg_config]: application-level messages encoding, and version parameters
    *)

val connect :
  net:('a, 'b, 'c) Tezos_p2p.P2p.t ->
  net_address:Messages.Ip_addr.t * int ->
  ('a, 'b, 'c) Tezos_p2p.P2p.connection option Lwt.t
(** Returns a connection for a given network address. *)

val connect_res :
  net:('a, 'b, 'c) Tezos_p2p.P2p.t ->
  net_address:Messages.Ip_addr.t * int ->
  ('a, 'b, 'c) Tezos_p2p.P2p.connection tzresult Lwt.t
(** Returns a connection for a given network address. *)

val connect_to_node :
  net:('a, 'b, 'c) P2p.t ->
  node:Messages.node ->
  ('a, 'b, 'c) P2p.connection option Lwt.t
(** Returns a connection for a given node. *)

val connect_to_node_res :
  net:('a, 'b, 'c) P2p.t ->
  node:Messages.node ->
  ('a, 'b, 'c) P2p.connection tzresult Lwt.t
(** Returns a connection for a given node. *)
