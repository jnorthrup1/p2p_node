(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Lib_dht lets you configure and run a network for a distributed hash
table. It provides the usual [store] and [get] operations from a DHT, both for a
local node (initialized and running with the {{!val:Lib_dht.init} [init]} 
and {{!val:Lib_dht.run} [run]} functions) and a remote node through remote
procedure calls (RPCs). *)

open Tezos_base.TzPervasives

type t
(** The type of a node from the distributed hash table. *)

(** The identifier of a node. *)
module Node_id : sig
  type t

  val digest : string -> t
  (** [digest path] computes the digest of a file at the given [path] using block-based hashing. *)

  (** {2 Comparing IDs } *)

  val compare : t -> t -> int

  val ( < ) : t -> t -> bool

  val ( > ) : t -> t -> bool

  val ( = ) : t -> t -> bool
  (** Structural equality of two IDs. *)

  val ( <> ) : t -> t -> bool
  (** Structural inequality of two IDs. *)

  val equal : t -> t -> bool

  (** {2 Binary-to-text encoding} *)

  val to_b58check : t -> string
  (** Returns a base 58 {{:https://en.wikipedia.org/wiki/Binary-to-text_encoding} binary-to-text encoding} of the hash. *)

  val of_b58check_exn : string -> t
  (** Returns the node ID from its base 58 encoding. *)
end

(** A network address (host IP address and TCP port). *)
module Net_address : sig
  type t

  val make : ip:string -> port:int -> t
  (** Returns a node network address from an IP and port number. *)

  val to_string : t -> string
  (** String representation of a network address. *)
end

(** {2 Manage a node} 
Configure and run a node. *)

val init :
  listening:Net_address.t ->
  bootstrap:Net_address.t option ->
  rpc_listening:Net_address.t ->
  t tzresult Lwt.t
(** [init ~listening ~bootstrap ~rpc_listening] initializes and returns a node.
- The node is configured to wait for connections at the [listening] address.
- [bootstrap] is the network address of the bootstrapping node (an existing node
that serves as a gateway to the network).
- [rpc_listening] corresponds to the listening address of the RPC server. *)

val run : t -> unit tzresult Lwt.t
(** Runs a given node. *)

(** {2 Interact with the DHT}
Store and retrieve data from the DHT. *)

val store : rpc_host:Net_address.t -> string -> Node_id.t tzresult Lwt.t
(** [store ~rpc_host path] stores the content of a file located in [path].
It returns the digest of the file for later retrieval.
The [rpc_host] corresponds to the network address of a running node RPC server. *)

val store_local : t -> string -> Node_id.t tzresult Lwt.t
(** [store_local t path] stores the content of a file located in [path].
It returns the digest of the file for later retrieval. *)

val get :
  rpc_host:Net_address.t ->
  key:string ->
  destination:string ->
  unit tzresult Lwt.t
(** [get ~rpc_host key destination] retrieves the file with digest [key] and writes to
the [destination] file path.
[rpc_host] corresponds to the network address of a running node RPC server. *)

val get_local :
  rpc_host:Net_address.t ->
  t ->
  key:string ->
  destination:string ->
  unit tzresult Lwt.t
(** [get_local ~rpc_host key destination] retrieves the file with digest [key] and writes to
the [destination] file path. *)

(** {2 Node introspection} 
Inspect any node in the network. *)
