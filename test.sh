#! /bin/sh

rm -rf _coverage
mkdir _coverage
BISECT_FILE=$(pwd)/_coverage/ dune test -f --instrument-with bisect_ppx
BISECT_FILE=$(pwd)/_coverage/ dune exec tests/main.exe  --  --info --verbose
rm 10* && rm id*
bisect-ppx-report html -o _coverage_report --coverage-path _coverage/
