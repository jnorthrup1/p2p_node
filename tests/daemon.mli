(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020-2021 Nomadic Labs <contact@nomadic-labs.com>           *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module type PARAMETERS = sig
  type persistent_state

  type session_state

  val base_default_name : string

  val default_colors : Log.Color.t array
end

module Make : functor (X : PARAMETERS) -> sig
  exception
    Terminated_before_event of {
      daemon : string;
      event : string;
      where : string option;
    }

  type session_status = {
    process : Process.t;
    session_state : X.session_state;
    mutable event_loop_promise : unit Lwt.t option;
  }

  type status = Not_running | Running of session_status

  type event_handler =
    | Event_handler : {
        filter : JSON.t -> 'a option;
        resolver : 'a option Lwt.u;
      }
        -> event_handler

  type event = { name : string; value : JSON.t }

  type t = {
    name : string;
    color : Log.Color.t;
    path : string;
    persistent_state : X.persistent_state;
    mutable status : status;
    event_pipe : string;
    mutable stdout_handlers : (string -> unit) list;
    mutable persistent_event_handlers : (event -> unit) list;
    mutable one_shot_event_handlers : event_handler list String_map.t;
  }

  val name : t -> string

  val terminate : t -> unit Lwt.t

  val next_name : int ref

  val fresh_name : unit -> string

  val next_color : int ref

  val get_next_color : unit -> Log.Color.t

  val create :
    path:string ->
    ?runner:Runner.t ->
    ?name:string ->
    ?color:Log.Color.t ->
    ?event_pipe:string ->
    X.persistent_state ->
    t

  val handle_raw_event : t -> string -> unit

  val run :
    ?runner:Runner.t ->
    ?on_terminate:(Unix.process_status -> unit Lwt.t) ->
    ?event_level:string ->
    t ->
    X.session_state ->
    string list ->
    unit Lwt.t

  val wait_for :
    ?where:string -> t -> string -> (JSON.t -> 'a option) -> 'a Lwt.t

  val on_event : t -> (event -> unit) -> unit

  val on_stdout : t -> (string -> unit) -> unit

  val log_events : t -> unit
end
